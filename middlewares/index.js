import jwt from "jsonwebtoken"
import { User } from "../models/index.js"
import { secret } from "../config.js"


export const checkAuthorization = (request, response, next) => {

    const authorization = request.headers.authorization
    const [Bearer, token] = authorization ? authorization.split(' '): [undefined, undefined]
    if (
        authorization!==undefined &&
        Bearer === 'Bearer' && token.length>0
    ) {
        jwt.verify(
            request.headers.authorization.split(' ')[1],
            secret,
            {},
            async (err, payload) => {
                console.log("payload", payload)
                if (err) {
                    return response.status(400).json({
                        message: 'Bad or expired token'
                    });
                }

                let user = undefined
                try {

                    user = await User.findUnique({
                        where: {
                            id: payload.id
                        }
                    })

                    console.log("user in databae", user)
                } catch(error) {
                    console.log(error)
                }

                if (user === null) {

                    response.status(400).json({
                        message: 'Bad token or user no longer exists'
                    });
                } else {
                    request.user = user;
                    next();
                }
            }
        );
    } else {
        response.status(401).json({
            message: 'Missing Bearer token'
        });
    }
};
