import express from 'express'
import cors from 'cors'
import router from './routes/index.js'
import rateLimit from 'express-rate-limit'
import { port } from './config.js'


const app = express()

const limiter = rateLimit({
	windowMs: 2 * 60 * 1000, // 2 minutes
	max: 150, // Limit each IP to 150 requests per `window` (here, per 2 minutes)
})
app.use(limiter)
app.use(express.json())
app.use(cors())
app.use(router)
app.listen(port, () => {
  console.log(`app listening on port ${port}`)
})

