import backgrounds from "../utils/backgrounds.js"
import crypto from "crypto"

// removes dashes from uuid present in the array of list objects
export function removeDashesFromIDs(lists, keynames) {
    return lists.map(list => {
        const tranformedUUIDS = {}

        for (let index = 0; index < keynames.length; index++) {
            tranformedUUIDS[keynames[index]] = list[keynames[0]].replaceAll('-', '')
            
        }
        return {
            ...list, ...tranformedUUIDS
        }
    }
    )
}


// adds dashes to uuid without dashes for proper matching of uuid in database
export function generatedashedUUID(uuid) {
    let newUUID = ""
    for (let index = 0; index < uuid.length; index++) {
        newUUID += uuid[index]
        if (index == 7)
            newUUID+='-'
        else if (index == 11)
            newUUID+='-'
        else if (index == 15)
            newUUID+='-'
        else if (index == 19)
            newUUID+='-'
    }
    return newUUID
}

// returns a random image from an array of immages present in the background.js file.
export function getRandomImage() {
    return backgrounds[parseInt(Math.random()*(backgrounds.length-1))]
}

export function generateHash(rawPassword) {
    return crypto.createHash("sha256").update(rawPassword, "utf8").digest("hex");
}