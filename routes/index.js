import express from 'express'
import controllers from '../controllers/index.js'
import {checkAuthorization} from "../middlewares/index.js"
const router = express.Router()




router.get('/users', controllers.getUsers)
router.post('/register', controllers.register)
router.post('/login', controllers.login)

router.get('/boards',checkAuthorization, controllers.getAllBoards)
router.post('/boards',checkAuthorization, controllers.createNewBoard)

router.get('/boards/:idBoard/lists',checkAuthorization, controllers.getAllLists)
router.post('/boards/:idBoard/:name',checkAuthorization, controllers.createNewList)
router.put('/lists/:idList/closed', checkAuthorization, controllers.archiveList)


router.get('/lists/:idList/cards',checkAuthorization, controllers.getAllCards)
router.post('/lists/:idList',checkAuthorization, controllers.createNewCard)
router.delete('/cards/:idCard',checkAuthorization, controllers.deleteCard)


router.get('/cards/:idCard/checklists',checkAuthorization, controllers.getAllCheckLists)
router.post('/cards/:idCard/checklists',checkAuthorization,controllers.createNewCheckList)
router.delete('/checklists/:idChecklist',checkAuthorization,controllers.deleteCheckList)

router.get('/checklists/:idChecklist/checkItems', checkAuthorization,controllers.getAllCheckItems)
router.post('/checklists/:idChecklist/checkItems',checkAuthorization, controllers.createNewCheckItem)
router.delete('/checkitems/:idCheckitem',checkAuthorization, controllers.deleteCheckItem)
router.put('/checkitems/:idCheckitem',checkAuthorization, controllers.updateCheckItem)

export default router