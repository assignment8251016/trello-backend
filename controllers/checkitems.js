import { stat } from "fs";
import { CheckItem, statusENUM } from "../models/index.js";
import { removeDashesFromIDs, generatedashedUUID } from '../utils/index.js'


export async function getAllCheckItems(request, response) {
    const { idChecklist } = request.params
  
    let checkitems = undefined
    try {
        
        checkitems = await CheckItem.findMany({
            where: { "idChecklist":  idChecklist},
        });
        
        response
            .status(200)
            .json(checkitems)
        
    } catch (error) {
        response
        .status(500).json({
            message: error.toString()
        })
    }

}


export async function createNewCheckItem(request, response) {
    const { idChecklist } = request.params
    const { name } = request.query
    
    let newCheckItem = undefined
       
    try {
        
        newCheckItem = await CheckItem.create({
            data: {
                name: name,
                idChecklist: idChecklist
            }
        })
        
        response
            .status(201)
            .json(newCheckItem)

    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }
    
}

export async function deleteCheckItem(request, response) {
    const { idCheckitem } = request.params

    
    let newCheckItem = undefined;
    try {
        
        newCheckItem = await CheckItem.delete({
            where: {
                id: idCheckitem,
            }
        })
        response
            .status(201)
            .json(newCheckItem)
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }
}


export async function updateCheckItem(request, response) { 
    const { status } = request.query
    const { idCheckitem } = request.params

    let updatedCheckitem = undefined
    try {
        updatedCheckitem  = await CheckItem.update({
            where: {
                id: idCheckitem,
            },
            data: {
                status: status==='complete'?statusENUM.complete:statusENUM.incomplete
            }
        })
        response.status(200).json(updatedCheckitem)
        
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }
    

}