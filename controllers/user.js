import jwt from "jsonwebtoken"

import { User } from "../models/index.js";
import { generateHash } from '../utils/index.js'
import { secret } from "../config.js";

export async function getUsers(request, response) {
    const users = await User.findMany();
    response.status(200).json(users)
}

export  async function register(request, response) {
    const newUser = request.body;
    let user = await User.findFirst({
        where: {
            email: newUser.email
        }
    })
    
    if (user === null) {
        

        user = await User.create({
            data: {
                ...newUser,
                password: generateHash(newUser.password),
            }
        })

        response.status(200).json(user)

    } else {

        response.status(400).json({
            message: "user already exists."
        })
    }
}

export async function login(request, response) {
    const loginUser = request.body;

    let user = await User.findFirst({
        where: {
            email: loginUser.email
        }
    })


    if (user !== null) {
        console.log("user about to login in is",  user)
        if (generateHash(loginUser.password) === user.password) {

            const token = jwt.sign({ id: user.id, username: user.username }, secret, {
                expiresIn: "1m",
            });
            
            response.status(201).json({
                id: user.id,
                username: user.username,
                email: user.email,
                token: token,
            })
        } else {
            response.status(400).json({
                message: "Wrong username or password"
            })
        }
    } else {

        response.status(400).json({
            message: "user does not exists, Please register"
        })
    }
}