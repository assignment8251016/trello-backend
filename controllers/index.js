import {getUsers, register, login} from './user.js'
import { getAllBoards, createNewBoard } from "./boards.js";
import { getAllLists, createNewList, archiveList } from "./lists.js";
import { getAllCards, createNewCard, deleteCard } from './cards.js';
import { getAllCheckLists, createNewCheckList, deleteCheckList } from './checklists.js'
import { getAllCheckItems, createNewCheckItem, deleteCheckItem, updateCheckItem } from './checkitems.js'


export default {
    register, login, getUsers,
    getAllBoards, createNewBoard,
    getAllLists, createNewList, archiveList,
    getAllCards, createNewCard, deleteCard,
    getAllCheckLists, createNewCheckList, deleteCheckList,
    getAllCheckItems, createNewCheckItem, deleteCheckItem, updateCheckItem
}
