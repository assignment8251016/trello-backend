import { Card } from "../models/index.js";

export async function getAllCards(request, response) {
    const {idList} = request.params

    let cards = undefined
    try {
        
        cards = await Card.findMany({
            where: { "idList":  idList},
        })

        response
        .status(200)
        .json(cards)
        
    } catch (error) {
        response
        .status(500).json({
            message: error.toString()
        })
    }

}

export async function createNewCard(request, response) {
    const {  name } = request.query
    const {  idList } = request.params
    let newCard = undefined
    try {  
        
        newCard = await Card.create({
            data: {
                name: name,
                idList: idList,
            }
        })
        response
        .status(201)
        .json(newCard)
        
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }

}

export async function deleteCard(request, response) {
    const { idCard } = request.params
    let newCard = undefined

    try {
        newCard = await Card.delete({
            where: {
                id: idCard
            }
        })
    
        response
        .status(200)
        .json(newCard)
        
    } catch (error) {

        response
        .status(400).json({
            message: error.toString()
        })

    }
}
