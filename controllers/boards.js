import { Board } from "../models/index.js"
import { getRandomImage } from '../utils/index.js'

export async function getAllBoards(request, response) {

    let boards = undefined
    try {

        boards = await Board.findMany({
            where: {
                idUser:  request.user.id
            }
        });
        response
        .status(200)
        .json(boards)
        
    } catch (error) {

        response
        .status(500).json({
            message: error.toString()
        })
    }
}

export async function createNewBoard(request, response) {
    const { name } = request.query

    let newBoard = undefined;
    try {
        newBoard=await Board.create({
            data: {
                name: name,
                backgroundImage: getRandomImage(),
                idUser: request.user.id
            }
        })
        
        response
        .status(201)
        .json(newBoard)
        
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }
    
}

