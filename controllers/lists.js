import { List, ListArchives } from "../models/index.js";
import { removeDashesFromIDs, generatedashedUUID } from '../utils/index.js'

export async function getAllLists(request, response) {
    const {idBoard} = request.params
    let lists = undefined
    try {

        lists = await List.findMany({
            where: { "idBoard": idBoard},
        });
        
        // const modifiedLists = removeDashesFromIDs(lists, ['id', 'idBoard'])
        response
        .status(200)
        .json(lists)
        
    } catch (error) {
        response
        .status(500).json({
            message: error.toString()
        })
    }
}

export async function createNewList(request, response) { 
    const { idBoard, name } = request.params

    let newList = undefined
    try {

        newList = await List.create({
            data: {
                name: name,
                idBoard: idBoard
            }
        })
    
        
        response
        .status(201)
        .json(newList)
        
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }
}


export async function archiveList(request, response) {
    const { idList } = request.params

    
    let deletedList = undefined
    try {

        deletedList = await List.delete({
            where: { id: idList}
        })

        await ListArchives.create({
            data:{
                ...deletedList,
                archived: true
            }
        })
        
        response
            .status(200)
            .json({...deletedList, archived: true })
        
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }

    
}