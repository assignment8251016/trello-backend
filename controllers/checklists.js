import { CheckList } from "../models/index.js";
import { removeDashesFromIDs, generatedashedUUID } from '../utils/index.js'

export async function getAllCheckLists(request, response) {
    const {idCard} = request.params
    
    let checklists = undefined
    try {
        checklists = await CheckList.findMany({
            where: { "idCard":  idCard},
        });
        
        
        response
        .status(200)
        .json(checklists)
        
    } catch (error) {
        response
        .status(500).json({
            message: error.toString()
        })
    }
}

export async function createNewCheckList(request, response) {
    const { idCard } = request.params
    const { name } = request.query
    
    let newCheckList = undefined
    try {

        newCheckList = await CheckList.create({
            data: {
                name: name,
                idCard: idCard
            }
        })
    
        response
        .status(201)
        .json(newCheckList)
        
    } catch (error) {

        response
        .status(400).json({
            message: error.toString()
        })

    }
}
export async function deleteCheckList(request, response) {
    const { idChecklist } = request.params
    
    let deletedCheckList = undefined
    try {

        deleteCheckList = await CheckList.delete({
            where: {
                id: idChecklist
            }
        })
    
        response
        .status(200)
        .json(deletedCheckList)
        
    } catch (error) {
        response
        .status(400).json({
            message: error.toString()
        })
    }
}

