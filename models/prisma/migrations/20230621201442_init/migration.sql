/*
  Warnings:

  - You are about to drop the column `idCard` on the `CheckItem` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "CheckItem" DROP CONSTRAINT "CheckItem_idCard_fkey";

-- AlterTable
ALTER TABLE "CheckItem" DROP COLUMN "idCard";
