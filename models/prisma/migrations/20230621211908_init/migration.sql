-- AlterTable
ALTER TABLE "Card" ADD COLUMN     "listArchivesId" TEXT;

-- CreateTable
CREATE TABLE "listArchives" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "archived" BOOLEAN NOT NULL,
    "idBoard" TEXT NOT NULL,

    CONSTRAINT "listArchives_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Card" ADD CONSTRAINT "Card_listArchivesId_fkey" FOREIGN KEY ("listArchivesId") REFERENCES "listArchives"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "listArchives" ADD CONSTRAINT "listArchives_idBoard_fkey" FOREIGN KEY ("idBoard") REFERENCES "Board"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
