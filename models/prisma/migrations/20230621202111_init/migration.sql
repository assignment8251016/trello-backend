/*
  Warnings:

  - You are about to drop the column `idBoard` on the `Card` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Card" DROP CONSTRAINT "Card_idBoard_fkey";

-- AlterTable
ALTER TABLE "Card" DROP COLUMN "idBoard";
