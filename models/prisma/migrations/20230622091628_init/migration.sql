/*
  Warnings:

  - You are about to drop the column `listArchivesId` on the `Card` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Card" DROP CONSTRAINT "Card_listArchivesId_fkey";

-- AlterTable
ALTER TABLE "Card" DROP COLUMN "listArchivesId";
