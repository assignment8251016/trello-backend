import { PrismaClient, Status } from '@prisma/client'
const prisma = new PrismaClient()


export const User = prisma.user
export const Board = prisma.board
export const List = prisma.list
export const Card = prisma.card
export const CheckList = prisma.checkList
export const CheckItem = prisma.checkItem
export const ListArchives = prisma.listArchives
export const statusENUM = Status


